<?php
function renderCalendar()
{
    $MONTHSTODISPLAY = 3;

    $weekdays = array(
        0 => "monday",
        1 => "tuesday",
        2 => "wednesday",
        3 => "thursday",
        4 => "friday",
        5 => "saturday",
        6 => "sunday",
    );

    $date = date('d-m-Y', time());
    $month_start = intval(date('n', time()));
    $year_start = intval(date('Y', time()));

    $months_to_display = range($month_start, $month_start + $MONTHSTODISPLAY - 1);
    $months_to_display_years = array_fill(0, $MONTHSTODISPLAY, $year_start);

    $data = "";

    for ($i = 1; $i <= $MONTHSTODISPLAY; $i++) {
        if ($months_to_display[$i] > 12) {
            # FIXME: Wenn ich mehr als einen Jahresübergang habe, funktioniert das hier nicht ausreichend.
            $months_to_display[$i] = $months_to_display[$i] - 12;
            $months_to_display_years[$i] = $months_to_display_years[$i] + 1;
        }
    }

    $data .= "<div><p>Aktueller Tag: " . $date . "</p></div>";
    $data .= "<div>\n";

    for ($i = 0; $i < $MONTHSTODISPLAY; $i++) {
        $data .= "<div>\n";
        $data .= "<table>\n";
        $data .= '<caption>' . $months_to_display_years[$i] . '-' . $months_to_display[$i] . "</caption>\n";
        $data .= "<tr>\n";
        $data .= "<th>Wochen-Nr.</th>\n";
        $data .= "<th>Mo</th>\n";
        $data .= "<th>Di</th>\n";
        $data .= "<th>Mi</th>\n";
        $data .= "<th>Do</th>\n";
        $data .= "<th>Fr</th>\n";
        $data .= "<th>Sa</th>\n";
        $data .= "<th>So</th>\n";
        $data .= "</tr>\n";
        # FIXME: Kalenderwochen mit Tagen in meheren Monaten werden doppelt ausgegeben.
        $month_day_start = date('Y-m-01', strtotime($months_to_display_years[$i] . '-' . $months_to_display[$i]));
        $month_day_end = date('Y-m-t', strtotime($months_to_display_years[$i] . '-' . $months_to_display[$i]));
        $weeks_in_month = (date('W', strtotime($month_day_end))) - (date('W', strtotime($month_day_start))) + 1;
        for ($j = 0; $j < $weeks_in_month; $j++) {
            if (($j * 7 + 1) > date('t', strtotime($months_to_display_years[$i] . '-' . $months_to_display[$i]))) {
                $week_date = $months_to_display_years[$i] . '-' . $months_to_display[$i] . '-' . date('t', strtotime($months_to_display_years[$i] . '-' . $months_to_display[$i]));
            } else {
                $week_date = $months_to_display_years[$i] . '-' . $months_to_display[$i] . '-' . strval($j * 7 + 1);
            }
            $week_number = date('W', strtotime($week_date));
            $monday_date = date('Y-m-d', strtotime($months_to_display_years[$i] . 'W' . $week_number));
            $data .= "<tr>\n";
            $data .= '<td>' . $week_number . "</td>\n";
            for ($k = 0; $k < 7; $k++) {
                $today = date("Y-m-d", strtotime($weekdays[$k] . ' this week', strtotime($monday_date)));
                if ($today == date('Y-m-d', time())) {
                    $data .= '<td style="background-color:#7fff00">' . $today . "</td>\n";
                } else {
                    $data .= '<td>' . $today . "</td>\n";
                }
            }
            $data .= "</tr>\n";
        }
        $data .= "</table>\n";
        $data .= "</div>\n";
        $data .= "\n";
    }
    $data .= "</div>\n";
    return $data;
}
