<?php declare(strict_types=1);

final class CarClass
{
    public int $ID;
    public string $name;
}

final class AccessTry
{
    public int $accesstriesID;
    public string $timestamp;
    public int $chips_chipID;
    public string $accessresult;
    public string $dbuser;
}

final class Car
{
    public int $ID;
    public string $manufacturer;
    public string $model;
    public int $car_class_ID;
    public bool $rented;
    public int $location;
    public float $pricePerDay;
}

final class Carpark
{
    public int $ID;
    public string $name;
    public float $latitude;
    public float $longitude;
}

final class SupplementaryEquipment
{
    public int $ID;
    public string $name;
    public string $description;
}

// Although this is considered by some folks an anti-pattern, I will just use it here because it is simple to implement
// and use.

final class DatabaseConnectionNettmann
{
    private static ?DatabaseConnectionNettmann $instance = null;
    private string $mysql_host;
    private string $mysql_db;
    private string $mysql_user;
    private string $mysql_password;
    private PDO $dbautohausnettmann;

    /**
     * gets the instance via lazy initialization (created on first usage)
     */
    public static function getInstance(): DatabaseConnectionNettmann
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct()
    {
        $this->mysql_host = 'mysql:host=' . getenv('MYSQL_HOST') . ';';
        $this->mysql_db = 'dbname=' . getenv('MYSQL_DATABASE');
        $this->mysql_user = getenv('MYSQL_USER');
        $this->mysql_password = getenv('MYSQL_PASSWORD');
        $this->dbautohausnettmann = new PDO($this->mysql_host . $this->mysql_db, $this->mysql_user, $this->mysql_password);
    }

    /**
     * prevent the instance from being cloned (which would create a second instance of it)
     */
    private function __clone()
    {
    }

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup()
    {
    }

    function getUserData($username)
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->prepare('SELECT userID, lastname, surname, `group`, password FROM users WHERE lastname = :username');
        $statement->bindParam(':username', $username);
        if ($statement->execute()) {
            $result = $statement->fetch();
        } else {
            $result = "";
        }
        $statement = null;
        return $result;
    }

    function getAccessTries()
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->query('SELECT accesstriesID, `timestamp`, chips_chipID, accessresult, dbuser FROM access_tries');
        $result = $statement->fetchAll(PDO::FETCH_CLASS, "AccessTry");
        $statement = null;
        return $result;
    }

    function getCarClasses()
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->query('SELECT ID, name FROM car_classes');
        $result = $statement->fetchAll(PDO::FETCH_KEY_PAIR);
        $statement = null;
        return $result;
    }

    function getCarClassById($id)
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->prepare('SELECT ID, name FROM car_classes WHERE ID = :class_id');
        $statement->bindParam(':class_id', $id);
        $statement->setFetchMode(PDO::FETCH_CLASS, "CarClass");
        if (!$statement->execute()) {
            return null;
        }
        $result = $statement->fetch();
        $statement = null;
        return $result;
    }

    function getCars()
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->query('SELECT ID, manufacturer, model, car_class_ID, rented, location, pricePerDay FROM cars');
        $statement->setFetchMode(PDO::FETCH_CLASS, "Car");
        $result = $statement->fetchAll();
        $statement = null;
        return $result;
    }

    function getCarById($id)
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->prepare('SELECT ID, manufacturer, model, car_class_ID, rented, location, pricePerDay FROM cars WHERE ID = :car_id');
        $statement->bindParam(':car_id', $id);
        $statement->setFetchMode(PDO::FETCH_CLASS, "Car");
        if (!$statement->execute()) {
            return null;
        }
        $result = $statement->fetch();
        $statement = null;
        return $result;
    }

    function getCarsOfClass($class_id)
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->prepare('SELECT ID, manufacturer, model, car_class_ID, rented, location, pricePerDay FROM cars WHERE car_class_ID = :class_ID');
        $statement->bindParam(':class_ID', $class_id);
        $statement->setFetchMode(PDO::FETCH_CLASS, "Car");
        if (!$statement->execute()) {
            return null;
        }
        $result = $statement->fetchAll();
        $statement = null;
        return $result;
    }

    function getSupplementaryEquipment()
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->query('SELECT ID, name, description FROM suplementary_equipment');
        if ($statement == false) {
            return null;
        }
        return $statement->fetchAll(PDO::FETCH_CLASS, "SupplementaryEquipment");
    }

    function getSupplementaryEquipmentOfCar($car_id)
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->prepare('SELECT suplementary_equipment.ID, name, description FROM suplementary_equipment INNER JOIN cars_supplementary_equipment ON suplementary_equipment.ID = cars_supplementary_equipment.supplementary_equipment_ID WHERE car_ID = :car_id');
        $statement->bindParam(':car_id', $car_id);
        $result_successful = $statement->execute();
        if (!$result_successful) {
            return null;
        }
        $result = $statement->fetchAll(PDO::FETCH_CLASS, "SupplementaryEquipment");
        $statement = null;
        return $result;
    }

    function getCarParks()
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->query('SELECT ID, name, latitude, longitude FROM carparks');
        if ($statement == false) {
            return null;
        }
        return $statement->fetchAll(PDO::FETCH_CLASS, "Carpark");
    }

    function getCarParkById($id)
    {
        $db = $this->dbautohausnettmann;
        $statement = $db->prepare('SELECT ID, name, latitude, longitude FROM carparks WHERE ID = :carpark_id');
        $statement->bindParam(':carpark_id', $id);
        $statement->setFetchMode(PDO::FETCH_CLASS, "Carpark");
        if (!$statement->execute()) {
            return null;
        }
        $result = $statement->fetch();
        $statement = null;
        return $result;
    }
}
