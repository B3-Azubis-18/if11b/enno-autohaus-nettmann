<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// First include all files. If all files are correctly included then $html_content .=  the content.
// We do not want to use require as this throws an error and exits the application. We want to give the user a hint
// we he is not seeing anything.

if (!@include "constants.php") {
    $html_content .=  "Konstanten-Include konnte nicht gefunden werden. Bitte repariere das Applikationssetup!";
    exit(1);
}

if (!@include "layout/header.php") {
    $html_content .=  "Header-Include konnte nicht gefunden werden. Bitte repariere das Applikationssetup!";
    exit(1);
}

if (!@include "layout/footer.php") {
    $html_content .=  "Footer-Include konnte nicht gefunden werden. Bitte repariere das Applikationssetup!";
    exit(1);
}

if (!@include "welcome.php") {
    $html_content .=  "Welcome-Include konnte nicht gefunden werden. Bitte repariere das Applikationssetup!";
    exit(1);
}

if (!@include "login.php") {
    $html_content .=  "Login-Include konnte nicht gefunden werden. Bitte repariere das Applikationssetup!";
    exit(1);
}

if (!@include "rentalcar.php") {
    $html_content .=  "Mietwagen-Include konnte nicht gefunden werden. Bitte repariere das Applikationssetup!";
    exit(1);
}

if (!@include "calendar.php") {
    $html_content .=  "Kalendar-Include konnte nicht gefunden werden. Bitte repariere das Applikationssetup!";
    exit(1);
}

if (!@include "buildingAccessControl.php") {
    $html_content .=  "Zutrittsversuchs-Include konnte nicht gefunden werden. Bitte repariere das Applikationssetup!";
    exit(1);
}

if (!@include "database.php") {
    $html_content .=  "Database-Include konnte nicht gefunden werden. Bitte repariere das Applikationssetup!";
    exit(1);
}

$html_head = '<html lang="de"><head><meta charset="utf-8">';
$html_head .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
$html_head .= '<link rel="stylesheet" type="text/css" href="/css/main.css">';
$html_head .= '<link rel="stylesheet" type="text/css" href="/css/rental-form.css"><script src="js/main.js">//Text';
$html_head .= '</script><title>Autohaus Nettmann</title></head><body>';

$html_end = '</body></html>';

function GoToNow($url)
{
    return '<script language="javascript">window.location.href ="' . $url . '"</script>';
}

// Update if present or set defaults
if (isset ($_SESSION['username'])) {
    $username = $_SESSION['username'];
} else {
    $username = "";
}
if (isset ($_SESSION['userID'])) {
    $userID = $_SESSION['userID'];
} else {
    $userID = "";
}
if (isset ($_SESSION['group'])) {
    $group = $_SESSION['group'];
} else {
    $group = "";
}
if (isset ($_GET["page"])) {
    $path = $_GET["page"];
} else {
    $path = "";
}

if (isset($_POST["btnLogin"])) {
    if (checkLoginData($_POST["username"], $_POST["password"])) {
        $username = $_POST["username"];
        $db_instance = DatabaseConnectionNettmann::getInstance();
        $user = $db_instance->getUserData($username);

        $group = $user["group"];
        $userID = $user["userID"];
        $_SESSION["username"] = $username;
        $_SESSION["userID"] = $userID;
        $_SESSION["group"] = $user["group"];
        echo GoToNow("/welcome");
    } else {
        $login_error_message = "Login failed!";
    }
}

// $html_content .=  the content.
$html_content = "";
$html_content .= getHeader();
$html_content .=  getNavbar($group, $username);
$html_content .=  "<div id='content' class='flex-container centerFlexbox'>";
if ($path == "welcome" or $path == "") {
    $html_content .=  getWelcome();
} elseif ($path == "login") {
    if (isset($login_error_message)) {
        $html_content .=  zeigeLoginFormular($login_error_message);
    } else {
        $html_content .=  zeigeLoginFormular();
    }
} elseif ($path == "logout") {
    if (session_status() == PHP_SESSION_ACTIVE) {
        endSession();
    }
} elseif ($path == "booking") {
    $html_content .=  showRentalCarForm();
} elseif ($path == "showReciept") {
    $html_content .=  showReciept();
} elseif ($path == "calendar") {
    $html_content .=  renderCalendar();
} elseif ($path == "building access") {
    $html_content .=  showAccessTries();
} else {
    $html_content .=  "Entschuldigung! Da ist wohl etwas unerwartetes passiert!";
}
$html_content .=  "</div>";
$html_content .=  getFooter();
$html_full = $html_head . $html_content . $html_end;

// https://stackoverflow.com/a/34556900/4730773
$dom = new DOMDocument();
$dom->preserveWhiteSpace = false;
$dom->formatOutput = true;
// https://stackoverflow.com/a/9149241/4730773
libxml_use_internal_errors(true);
$dom->loadHTML($html_full,LIBXML_HTML_NOIMPLIED);
libxml_use_internal_errors(false);
$html_result = $dom->saveXML($dom->documentElement, LIBXML_NOEMPTYTAG);
$html_result = preg_replace('~></(?:area|base(?:font)?|br|col|command|embed|frame|hr|img|input|keygen|link|meta|param|source|track|wbr)>\b~i', '/>', $html_result);

// https://stackoverflow.com/a/17138061/4730773
echo "<!DOCTYPE html>\n";
echo $html_full;